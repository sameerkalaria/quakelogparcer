const fs = require("fs");

// Reading log file and getting all logs in array
const readDataFromLogFile = function (filePath) {
  const log = fs.readFileSync(filePath, "utf-8");
  const lines = log.split("\n");
  return lines;
};

// Getting all game data in object
const getEachGameData = function (logs) {
  // Geeting each game data
  let eachGameData = {};
  let counter = 0;
  for (let line of logs) {
    const data = line.split(" ");
    if (data[2] === "InitGame:") {
      counter += 1;
      eachGameData[`game_${counter}`] = [];
    }

    if (eachGameData.hasOwnProperty(`game_${counter}`)) {
      eachGameData[`game_${counter}`].push(line);
    }
  }

  return eachGameData;
};

// Collecting required information from each game
const groupAllData = function (singleGameLogs) {
  let info = {
    total_kills: 0,
    players: [],
    kills: {},
  };

  for (const line of singleGameLogs) {
    const data = line.split(" ");

    if (data[2] === "Kill:") {
      info.total_kills += 1;

      if (data[6] === "<world>") {
        if (info.kills.hasOwnProperty(`${data[8]}`)) {
          info.kills[`${data[8]}`] -= 1;
        } else {
          info.kills[`${data[8]}`] = -1;
        }
      } else if (!info.players.includes(data[6])) {
        info.players.push(data[6]);
        info.kills[`${data[6]}`] = 1;
      } else {
        info.kills[`${data[6]}`] += 1;
      }
    }
  }

  return info;
};

// Grouping all game data in object
const getGroupedInfo = function (eachGameData) {
  let groupInfo = {};
  for (const key in eachGameData) {
    const singleGameLogs = eachGameData[key];

    // grouping information
    const info = groupAllData(singleGameLogs);

    groupInfo[`${key}`] = info;
  }

  return groupInfo;
};

// Getting particular game grouped info
const getGameGroupedInfo = function (gameName, groupInfo) {
  const gameInfo = {};
  for (const key in groupInfo) {
    if (key === gameName) {
      return groupInfo[`${key}`];
    }
  }

  return `There is no any game with name '${gameName}' in logs`;
};

// Getting count of death group by cause for single game
const deathGroupByCause = function (singleGameLogs) {
  let death = {
    kills_by_means: {},
  };

  for (const line of singleGameLogs) {
    const data = line.split(" ");
    if (data[2] === "Kill:") {
      if (death.kills_by_means.hasOwnProperty(`${data.slice(-1)}`)) {
        death.kills_by_means[`${data.slice(-1)}`] += 1;
      } else {
        death.kills_by_means[`${data.slice(-1)}`] = 1;
      }
    }
  }
  return death;
};

// Grouping death group by cause for all game
const getDeathInfoGroupByCause = function (eachGameData) {
  let deathInfo = {};

  for (const key in eachGameData) {
    const singleGameLogs = eachGameData[key];

    const death = deathGroupByCause(singleGameLogs);

    deathInfo[`${key}`] = death;
  }

  return deathInfo;
};

// Task 1 : Log parser
// Read the log file
// Group the game data of each match
// Collect kill data

const logs = readDataFromLogFile(`${__dirname}/logs/qgames.log`);
const eachGameData = getEachGameData(logs);
fs.writeFileSync(`${__dirname}/output/task1.txt`, JSON.stringify(eachGameData));

// Task 2 : Create a script that prints a report (grouped information) for each match and a player ranking.
const groupInfo = getGroupedInfo(eachGameData);
console.log(groupInfo);
fs.writeFileSync(`${__dirname}/output/task2.txt`, JSON.stringify(groupInfo));

// Task 3 : Generate a report of deaths grouped by death cause for each match.
const deathInfo = getDeathInfoGroupByCause(eachGameData);
console.log(deathInfo);
fs.writeFileSync(`${__dirname}/output/task3.txt`, JSON.stringify(deathInfo));
